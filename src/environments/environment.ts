// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  Path:{
    url: 'http://localhost:4200/assets/'  
  },
  Api: {
    token: '8b432d51-5e1b-41e9-96f7-86857b61de87',
    url: 'https://fishingapp-e34e1.firebaseio.com/' //YOUR FIREBASE ENDPOINT
  
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
