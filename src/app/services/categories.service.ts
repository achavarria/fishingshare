import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get(`${environment.Api.url}categories.json`);
  }

  getTopCategories(number: number = 5) {
    return this.http.get(`${environment.Api.url}categories.json?orderBy="$key"&limitToFirst=${number}&startAt="0"&print=pretty`);
  }

  getFilterData(orderBy, equalTo) {
    return this.http.get(`${environment.Api.url}sub-categories.json?orderBy="${orderBy}"&equalTo="${equalTo}"&print=pretty`);

  }
}
