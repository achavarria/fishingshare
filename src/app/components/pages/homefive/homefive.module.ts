import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { HomefiveRoutingModule } from './homefive-routing.module';
import { HomefiveComponent } from './homefive.component';

import { SharedModule } from '../../shared/shared.module';
import { ContentComponent } from './content/content.component'

@NgModule({
  declarations: [HomefiveComponent, ContentComponent],
  imports: [
    CommonModule,
    HomefiveRoutingModule,
    SlickCarouselModule,
    NgbModule,
    SharedModule
  ]
})
export class HomefiveModule { }
