import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homefive',
  templateUrl: './homefive.component.html',
  styleUrls: ['./homefive.component.css']
})
export class HomefiveComponent implements OnInit {
  // Instagram
  instaclassname = "secondary-bg";
  // Footer style
  classname = "";
  ftlogo = "assets/img/logo.png"
  constructor() { }

  ngOnInit(): void {
  }

}
