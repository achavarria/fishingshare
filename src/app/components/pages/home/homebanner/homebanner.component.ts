import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-homebanner',
  templateUrl: './homebanner.component.html',
  styleUrls: ['./homebanner.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomebannerComponent implements OnInit {

	banner_home:Array<any> = [];
	myCategories:Array<any> = [];
	url:Array<any> = [];
	render:Boolean = true;
	preload:Boolean = false;
  
  topbannerConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    autoplay: true
  };

  constructor(
    private productService: ProductsService) { }

  ngOnInit(): void {

    this.preload = true;
    let index = 0;

		this.productService.getData()
		.subscribe(resp =>{
			
			/*=============================================
			Tomar la longitud del objeto
			=============================================*/

			let i;
			let size = 0;

			for(i in resp){
				size++
			}

			/*=============================================
			Generar un número aleatorio 
			=============================================*/

			if(size > 5){
				index = Math.floor(Math.random()*(size-5));
			}

			/*=============================================
			Seleccionar data de productos con límites
			=============================================*/


			this.productService.getLimitData(Object.keys(resp)[index], 5)
			.subscribe( resp => { 

				let i;

				for(i in resp){				
					this.banner_home.push(resp[i].horizontal_slider)
					this.myCategories.push(resp[i].category)
					this.url.push(resp[i].url)
					this.preload = false;
				}
			})

		})
  }

  callback(){
		if(this.render){
			this.render = false;
		}

	}

}
