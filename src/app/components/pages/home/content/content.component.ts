import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import shoppost from '../../../../data/shop.json'
import categorypost from '../../../../data/category.json'
import blogcategory from '../../../../data/blogcategory.json';
import blogpost from '../../../../data/blog.json';
import blogtags from '../../../../data/blogtags.json';
import services from '../../../../data/services.json';
import { ProductsService } from 'src/app/services/products.service';
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  closeResult: string;
  modalContent: any;

  banner_home: Array<any> = [];
  myCategories: Array<any> = [];
  url: Array<any> = [];
  render: Boolean = true;
  preload: Boolean = false;

  products: Array<any> = [];


  constructor(private modalService: NgbModal,
    private productService: ProductsService,
    private categoriesService: CategoriesService) { }


  open(content: any, item: any) {
    this.modalContent = item
    this.modalService.open(content, { centered: true, size: "lg", windowClass: 'andro_quick-view-modal p-0' });
  }
  // Increment decrement
  public counter: number = 1
  increment() {
    this.counter += 1;
  }
  decrement() {
    this.counter -= 1;
  }
  public sponsers = services;
  public shopbox: { img: string }[] = shoppost;
  public featuredpost: { img: string }[] = shoppost;
  public category: { icon: string }[] = categorypost;
  public blogcategory: { title: string }[] = blogcategory;
  public blogbox: { title: string, id: number }[] = blogpost;
  public tags: { title: string, id: number }[] = blogtags;
  public getBlogTags(items: string | any[]) {
    var elems = blogtags.filter((item: { id: string; }) => {
      return items.includes(item.id)
    });
    return elems;
  }
  // topBanner
  topbanner = [
    { img: "assets/img/home-slider-img-1.jpg" },
    { img: "assets/img/home-slider-img-2.jpg" },
    { img: "assets/img/home-slider-img-3.jpg" },
  ];
  topbannerConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    autoplay: true
  };
  // TopBanner Thumb
  topThumb = [
    {
      img: "assets/img/home-img-1.jpg",
      subtitle: "Comprar Ahora",
      title: "Fly Fishing",
      lasttitle: "Buscar Productos",
      Url: ''
    },
    {
      img: "assets/img/home-img-2.jpg",
      subtitle: "Comprar Ahora",
      title: "Baitcasting",
      lasttitle: "Buscar Productos",
      Url: ''
    },
    {
      img: "assets/img/home-img-3.jpg",
      subtitle: "Comprar Ahora",
      title: "Spinning",
      lasttitle: "Buscar Productos",
      Url: ''
    }
  ];
  // Sponsors
  sponsorConfig = {
    slidesToShow: 8,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    autoplay: true,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 6,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 400,
      settings: {
        slidesToShow: 2,
      }
    }
    ]
  };
  // Banner
  bannerConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    autoplay: true,
    prevArrow: '.banner-3 .slider-prev',
    nextArrow: '.banner-3 .slider-next',
  }
  // Featured
  newfeaturedpost: any[];
  splitArr(arr, size) {
    let newArr = [];
    for (let i = 0; i < arr.length; i += size) {
      newArr.push(arr.slice(i, i + size));
    }
    return newArr;
  }
  // Cta
  shopcta = [
    {
      photo: "assets/img/cta/1.jpg",
      title: "Modern Design",
      titlespan: "Gears",
      para: "Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus."
    },
    {
      photo: "assets/img/cta/2.jpg",
      title: "Modern Design",
      titlespan: "Fishinizerry",
      para: "Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus."
    }
  ];
  // Top Category
  topcategoryConfig = {
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    autoplay: false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          autoplay: true,
          arrows: false,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          arrows: false,
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      }
    ]
  }
  // Daily deals
  dealsConfig = {
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    autoplay: true,
    prevArrow: '.andro_fresh-arrivals .slider-prev',
    nextArrow: '.andro_fresh-arrivals .slider-next',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  }
  // Other Mentions
  otherConfig = {
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '.andro_other-mentions .slider-prev',
    nextArrow: '.andro_other-mentions .slider-next',
    dots: false,
    autoplay: false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 450,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  }


  ngOnInit(): void {
    this.newfeaturedpost = this.splitArr(this.featuredpost, 3);

    this.categoriesService.getData()
      .subscribe((resp: any) => {
        this.category = resp.sort((a, b) => a.category.localeCompare(b.category));
      })

    this.productService.getQuantityProdcuts(6).subscribe((resp: any) => {

      let i;

      for (i in resp) {
        if (resp[i]["offer"][2] != null && resp[i]["offer"][2] != undefined) {

          var end_offer = new Date(resp[i].end_offer);
          var toDate = new Date()

          var diff = Math.floor((Date.UTC(end_offer.getFullYear(), end_offer.getMonth(), end_offer.getDate()) - Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate())) / (1000 * 60 * 60));

          resp[i].timeleft = diff;
          resp[i].discount = Number(["offer"][1]);

        } else {
          resp[i].featured = true;
        }

        this.products.push(resp[i]);
      }
    });

  }

}
