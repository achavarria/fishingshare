import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { CountdownModule } from 'ngx-countdown';
import { NgMasonryGridModule } from 'ng-masonry-grid';
import { NiceSelectModule } from "ng-nice-select";

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

import { SharedModule } from '../../shared/shared.module';
import { ContentComponent } from './content/content.component';
import { HomebannerComponent } from './homebanner/homebanner.component';
import { FeaturesComponent } from './features/features.component';
import { HotTodayComponent } from './hot-today/hot-today.component'
import { CarouselModule } from 'ngx-owl-carousel-o';

@NgModule({
  declarations: [HomeComponent, ContentComponent, HomebannerComponent, FeaturesComponent, HotTodayComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SlickCarouselModule,
    NgbModule,
    CountdownModule,
    SharedModule,
    CarouselModule,
    NgMasonryGridModule,
    NiceSelectModule
  ],
  exports: [HomebannerComponent, FeaturesComponent, HotTodayComponent]
})
export class HomeModule { }
