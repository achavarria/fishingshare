import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { SalesService } from 'src/app/services/sales.service';
import { OwlCarouselConfig, CarouselNavigation, SlickConfig, ProductLightbox, CountDown, Rating, ProgressBar } from '../../../../functions';

import { OwlOptions } from 'ngx-owl-carousel-o';


declare var jQuery: any;
declare var $: any;



@Component({
  selector: 'app-hot-today',
  templateUrl: './hot-today.component.html',
  styleUrls: ['./hot-today.component.css']
})
export class HotTodayComponent implements OnInit {


  constructor(private productsService: ProductsService,
    private salesService: SalesService) { }

 // Banner
 bannerConfig = {
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
  autoplay: true,
  prevArrow: '.banner-3 .slider-prev',
  nextArrow: '.banner-3 .slider-next',
} 

  indexes: Array<any> = [];
  products: Array<any> = [];
  render: Boolean = true;
  cargando: Boolean = true;
  topSales: Array<any> = [];
  topSalesBlock: Array<any> = [];
  renderBestSeller: Boolean = true;

  ngOnInit(): void {

    this.cargando = true;

    let getProducts = [];
    let hoy = new Date();
    let fechaOferta = null;
    


    /*=============================================
  Tomamos la data de los productos
  =============================================*/

    this.productsService.getData()
      .subscribe(resp => {

        /*=============================================
        Recorremos cada producto para separar las ofertas y el stock
        =============================================*/



        let i;

        for (i in resp) {

          if (resp[i]["offer"][2] != null && resp[i]["offer"][2] != undefined) {

            var end_offer = new Date(resp[i].end_offer);
            var toDate = new Date()
    
            var diff = Math.floor((Date.UTC(end_offer.getFullYear(),end_offer.getMonth(),end_offer.getDate())-Date.UTC(toDate.getFullYear(),toDate.getMonth(),toDate.getDate()) )/(1000 * 60 * 60));

            resp[i].timeleft = diff;
            resp[i].featured = true;
            
            this.products.push(resp[i]);
          }
        }

        console.log(this.products)
      });


    /*=============================================
    Tomamos la data de las ventas
    =============================================*/

    let getSales = [];

    this.salesService.getData()
      .subscribe(resp => {

        /*=============================================
        Recorremos cada venta para separar los productos y las cantidades
        =============================================*/

        let i;

        for (i in resp) {
          getSales.push({
            "product": resp[i].product,
            "quantity": resp[i].quantity
          })

        }

        /*=============================================
        Ordenamos de mayor a menor el arreglo de objetos
        =============================================*/

        getSales.sort(function (a, b) {
          return (b.quantity - a.quantity)
        })

        /*=============================================
        Sacamos del arreglo los productos repetidos dejando los de mayor venta
        =============================================*/

        let filterSales = [];

        getSales.forEach(sale => {

          if (!filterSales.find(resp => resp.product == sale.product)) {
            const { product, quantity } = sale;
            filterSales.push({ product, quantity })
          }

        })


        /*=============================================
        Filtramos la data de productos buscando coincidencias con las ventas
        =============================================*/

        let block = 0;

        filterSales.forEach((sale, index) => {

          /*=============================================
          Filtramos hasta 20 ventas
          =============================================*/

          if (index < 20) {

            block++;

            this.productsService.getFilterData("name", sale.product)
              .subscribe(resp => {
                let i;

                for (i in resp) {
                  this.topSales.push(resp[i])
                }
              })
          }

        })

        /*=============================================
        Enviamos el máximo de bloques para mostrar 4 productos por bloque
        =============================================*/

        for (let i = 0; i < Math.ceil(block / 4); i++) {
          this.topSalesBlock.push(i);
        }

      }, (errr) => { console.log('salesService-error', errr) });

  }

  /*=============================================
  Función que nos avisa cuando finaliza el renderizado de Angular
  =============================================*/

  callback() {
    if (this.render) {
      this.render = false;
    }
  }

}
