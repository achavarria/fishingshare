import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { CountdownModule } from 'ngx-countdown';
import { NgMasonryGridModule } from 'ng-masonry-grid';

import { HomefourRoutingModule } from './homefour-routing.module';
import { HomefourComponent } from './homefour.component';

import { SharedModule } from '../../shared/shared.module';
import { ContentComponent } from './content/content.component'

@NgModule({
  declarations: [HomefourComponent, ContentComponent],
  imports: [
    CommonModule,
    HomefourRoutingModule,
    SlickCarouselModule,
    NgbModule,
    CountdownModule,
    SharedModule,
    NgMasonryGridModule
  ]
})
export class HomefourModule { }
