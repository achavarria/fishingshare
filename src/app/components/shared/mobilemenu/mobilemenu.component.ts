import { Component, OnInit } from '@angular/core';
import $ from 'jquery'
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
  selector: 'app-mobilemenu',
  templateUrl: './mobilemenu.component.html',
  styleUrls: ['./mobilemenu.component.css']
})
export class MobilemenuComponent implements OnInit {

  constructor(private categoriesService: CategoriesService) { }

  categories: Object = null;
  render: Boolean = true;
  categoriesList: Array<any> = [];


  ngOnInit(): void {
    function mobilemenu() {
      ($(".andro_aside .menu-item-has-children > a") as any).on('click', function (e) {
        var submenu = $(this).next(".sub-menu");
        e.preventDefault();

        submenu.slideToggle(200);
      });
    }
    mobilemenu()

    /*=============================================
        Tomamos la data de las categorías
        =============================================*/

    this.categoriesService.getData()
      .subscribe(resp => {

        this.categories = resp;

        /*=============================================
        Recorrido por el objeto de la data de categorías
        =============================================*/

        let i;

        for (i in resp) {

          /*=============================================
          Separamos los nombres de categorías
          =============================================*/

          this.categoriesList.push(resp[i].category)

        }

      })


    /*=============================================
        Activamos el efecto toggle en el listado de subcategorías
        =============================================*/

    $(document).on("click", ".sub-toggle", function () {
      $(this).parent().children('ul').toggle();

    })

  }


  /*=============================================
  Función que nos avisa cuando finaliza el renderizado de Angular
  =============================================*/

  callback() {

    if (this.render) {

      this.render = false;
      let arraySubCategories = [];

      /*=============================================
      Separar las categorías
      =============================================*/

      this.categoriesList.forEach(category => {

        /*=============================================
        Tomamos la colección de las sub-categorías filtrando con los nombres de categoría
        =============================================*/

        this.categoriesService.getFilterData("category", category)
          .subscribe(resp => {

            /*=============================================
            Hacemos un recorrido por la colección general de subcategorias y clasificamos las subcategorias y url
            de acuerdo a la categoría que correspondan
            =============================================*/

            let i;

            for (i in resp) {

              arraySubCategories.push({
                "category": resp[i].category,
                "subcategory": resp[i].name,
                "url": resp[i].url
              })

            }

            /*=============================================
            Recorremos el array de objetos nuevo para buscar coincidencias con los nombres de categorías
            =============================================*/

            for (i in arraySubCategories) {

              if (category == arraySubCategories[i].category) {

                $(`[category='${category}']`).append(
                  `<li class="current-menu-item menu-item">
		                        	<a href="products/${arraySubCategories[i].url}">${arraySubCategories[i].subcategory}</a>
		                        </li>`
                )
              }
            }
          })

      })

    }

  }



}

















