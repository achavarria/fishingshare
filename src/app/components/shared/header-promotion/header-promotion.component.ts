import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header-promotion',
  templateUrl: './header-promotion.component.html',
  styleUrls: ['./header-promotion.component.css']
})
export class HeaderPromotionComponent implements OnInit {


  path: String = environment.Path.url;
  top_banner: Object = null;
  preload: Boolean = false;
  category: Object = null;

  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {

    this.preload = true;

    this.productsService.getData()
      .subscribe(resp => {

        /*=============================================
        Tomar la longitud del objeto
        =============================================*/

        let i;
        let size = 0;

        for (i in resp) {

          size++

        }

        /*=============================================
        Generar un número aleatorio 
        =============================================*/

        let index = Math.floor(Math.random() * size);

        /*=============================================
        Devolvemos a la vista un banner aleatorio
        =============================================*/
        this.category =resp[Object.keys(resp)[index]].category;
        this.top_banner = resp[Object.keys(resp)[index]].top_banner;

        this.preload = false;


      })

  }

}
