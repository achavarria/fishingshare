import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { OwlCarouselConfig } from '../../../functions.js';

@Component({
  selector: 'app-horizontal-banner',
  templateUrl: './horizontal-banner.component.html',
  styleUrls: ['./horizontal-banner.component.css']
})
export class HorizontalBannerComponent implements OnInit {

  constructor(private productService: ProductsService) { }


	banner_home:Array<any> = [];
	myCategories:Array<any> = [];
	url:Array<any> = [];
	render:Boolean = true;
	preload:Boolean = false;

	ngOnInit(): void {

		this.preload = true;

		let index = 0;

		this.productService.getData()
		.subscribe(resp =>{
			
			/*=============================================
			Tomar la longitud del objeto
			=============================================*/

			let i;
			let size = 0;

			for(i in resp){

				size++			

			}

			/*=============================================
			Generar un número aleatorio 
			=============================================*/

			if(size > 5){

				index = Math.floor(Math.random()*(size-5));

			}

			/*=============================================
			Seleccionar data de productos con límites
			=============================================*/


			this.productService.getLimitData(Object.keys(resp)[index], 5)
			.subscribe( resp => { 

				let i;

				for(i in resp){
				
					this.banner_home.push(resp[i].horizontal_slider)
					this.myCategories.push(resp[i].category)
					this.url.push(resp[i].url)

					this.preload = false;

				}

			})

		})

	}

	/*=============================================
	Función que nos avisa cuando finaliza el renderizado de Angular
	=============================================*/
	
	callback(){

		if(this.render){

			this.render = false;

			OwlCarouselConfig.fnc()

		}

	}
}